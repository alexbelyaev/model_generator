'''
Алгоритм построения RDA карт для FMCW радара при помощи 3D FFT
Author: Trots D.O.
Комментарии: Очень долго!!!
'''

import numpy as np

from scipy import constants

import cv2 as cv
import matplotlib.pyplot as plt

################################

#T_sw = 3.5e-5
T_adc = 25.6e-6
T_puls = 30e-6
f_d = 10e6
f_c = 77e9

sol = 3e8
sof = 1e13

################################
##Задание начальных параметров##
################################
#Xt = (np.load('IF_signal_model.npy'))
sampls = int(T_adc*f_d)
pulses = 512
H = 128

num = 200

dD = 140/768
Dmax = 140
d_axe = np.arange(0,Dmax,dD)

Vmax = sol/(2*T_puls*f_c)
dV = sol/(2*f_c*T_puls*H)

df = 1/37
Pi = np.pi

freqq0 = np.arange(-0.86,0.86,1*df/2)
teth_axe0 = np.zeros(len(freqq0),float)
teth_axe0[:] = np.arcsin( (freqq0[:]))*180/Pi

V = np.arange(-Vmax/2,Vmax/2,dV/4)

resiv = 128

resiv0 = 512

N = num
for Z in range (38,39):
    
#    plt.close("all")
    
#    xt = np.loadtxt('D:\\work\\carla_locat_data\\out\\targets_out\\' + str(Z) + '_model.txt').view(complex) # Загрузка данных модели из C++ в виде одномерного массива
    
    xt = np.loadtxt('D:\\work\\locat\\FMCW-radar\\FMCW_radar\\dirty_beam.txt').view(complex)
    xt = np.reshape(xt, (8, H, 768))

    fftsamples = 768

    Xt0 = np.fft.fftn(xt,(fftsamples, pulses, resiv))

    print(Xt0.shape)
            
    for i in range(resiv):
        for j in range(fftsamples):
            Xt0[j,:,i] = np.fft.fftshift(Xt0[j,:,i])
        
    for i in range(pulses):
        for j in range(fftsamples):
            Xt0[j,i,:] = np.fft.fftshift(Xt0[j,i,:])
        
    Pi2 = 9.869604401089358

    koef = 16*dD*dD*Pi2
    Xt0 = abs(Xt0)

#    for i in range(resiv):
#        for j in range(pulses):
#            for k in range(sampls):
#                Xt0[i,j,k] = Xt0[i,j,k]*koef*k*np.sqrt(k)
    
#    minimum0 = (np.amin(np.real(Xt0)))
#    maximum0 = (np.amax(np.real(Xt0)))
#    Xt0 = Xt0/maximum0
    masdlk = 341
    max_d_num = int(512)

    d_axe_new = d_axe[0:max_d_num]

    res0 = Xt0[0:max_d_num,128,:]
    
    res_1 = np.copy(res0)
    
    
    veloc = 0
    for i in range(resiv):
        for j in range(sampls+10):
            veloc += (koef*j*j*Xt0[j,:,i])**3
            
    
    veloc = veloc/max(veloc)  
    veloc[int(pulses/2)] = (veloc[int(pulses/2)-1] + veloc[int(pulses/2)+1])/2

    if (pulses == 512):
        for i in range(-6,7,1):
            veloc[int(pulses/2)+i] = veloc[int(pulses/2)+i - 1]

#    for i in range(max_d_num):
#        for k in range(resiv):
#            res_1[k,i] = res0[k,i]*koef*i*np.sqrt(i)
            
    matoj = 10*np.log10(np.mean(veloc))
    minim = 10*np.log10(np.min(veloc))
    CKO   = 10*np.log10(np.std(veloc))
    
    treshold  = 0.05 #minim - 0.75*CKO
    
    veloc_out = np.copy(veloc)
    
#    for i in range(pulses):
#        if (10*np.log10(veloc[i]) < treshold):
#            veloc_out[i] = 1e-6
    
    for i in range(pulses):
        if (veloc[i] < treshold):
            veloc_out[i] = 1e-6
            
    for i in range(2,pulses-2):
        if( (veloc_out[i]>veloc_out[i+1]) & (veloc_out[i]>veloc_out[i-1]) ):
            if (veloc_out[i-1] > veloc_out[i-2] ):
                veloc_out[i-2] = 1e-6
            
            if (veloc_out[i+1] > veloc_out[i+2] ):
                veloc_out[i+2] = 1e-6
                
            veloc_out[i-1] = 1e-6 
            veloc_out[i+1] = 1e-6
            
            
    maxis = np.where(veloc_out>1e-6)
    

#    for i in range(pulses):
#            for j in range(sampls):
#                Xt0[0,i,j] = Xt0[0,i,j]*koef*j*np.sqrt(j)

    cmap = plt.get_cmap('jet')
    
    Xt0_new = Xt0[:,int(pulses/2),:] 
    
    Xt0_new = np.swapaxes(Xt0_new,0,1)

    plt.figure(2)
    X, Y = np.meshgrid(teth_axe0, d_axe)
    plt.pcolormesh( X, Y, np.fft.fftshift(Xt0_new), cmap = cmap)
    plt.colorbar()
    plt.title('2D FFT RAM')
    plt.ylabel('tetha, [grad]')
    plt.xlabel('range, [m]')

    vel_res = 0
    
#    cmap = plt.get_cmap('jet')
#    plt.figure(0)
#    X, Y = np.meshgrid(d_axe_new, teth_axe0)
#    plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), Xt0[:,int(pulses/2),:], cmap = cmap)
#    plt.colorbar()
##    plt.grid('on')
#    plt.title('2D FFT ESTIMATE Rx-Ry ' + 'on velocity equal ZERO ' + 'km/h')
#    plt.xlabel('range Y')
#    plt.ylabel('range X')
#    plt.ylim([0,150])
#    
    
#    len_xt1 = 256+10
#    
#    Xt1 = Xt0[:,:,0:len_xt1]
#
#    for i in range (len_xt1):
#        for j in range (resiv):
#            vel_res += (koef*i*i*Xt1[j,:,i])**3
#
#    vel_res = vel_res/max(vel_res)
#    
#    plt.figure(0)
#    plt.plot(V,20*np.log10(vel_res))
#    plt.grid('on')
#    plt.title('1d FFT ESTIMATE V')
#    plt.xlabel('velocity, [m/s]') 
#    
#    plt.figure(500)
#    plt.plot(V,10*np.log10(veloc))
#    plt.grid('on')
#    plt.title('1d FFT ESTIMATE V')
#    plt.xlabel('velocity, [m/s]') 
#    
#    veloc = veloc/max(veloc)
#    
#    plt.figure(501)
#    plt.plot(V,(veloc))
#    plt.grid('on')
#    plt.title('1d FFT ESTIMATE V')
#    plt.xlabel('velocity, [m/s]') 
#    
#    plt.figure(600)
#    plt.plot(V,10*np.log10(veloc_out))
#    plt.grid('on')
#    plt.title('1d FFT ESTIMATE V')
#    plt.xlabel('velocity, [m/s]')  
#
#    for i in range(resiv):
#            for j in range(fftsamples-100 ):
#                Xt0[i,int(pulses/2),j] = Xt0[i,int(pulses/2),j]*koef*j*np.sqrt(j)
#
#
#    cmap = plt.get_cmap('jet')
#    plt.figure(0)
#    X, Y = np.meshgrid(d_axe_new, teth_axe0)
#    plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), Xt0[:,int(pulses/2),:], cmap = cmap)
#    plt.colorbar()
##    plt.grid('on')
#    plt.title('2D FFT ESTIMATE Rx-Ry ' + 'on velocity equal ZERO ' + 'km/h')
#    plt.xlabel('range Y')
#    plt.ylabel('range X')
#    plt.ylim([0,150])
#    
#    for i in range(len(maxis[0])):
#            cmap = plt.get_cmap('jet')
#            plt.figure(i+1)
#            X, Y = np.meshgrid(d_axe_new, teth_axe0)
#            plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), Xt0[:,maxis[0][i],:], cmap = cmap)
#            plt.colorbar()
#            plt.title('2D FFT ESTIMATE ' + 'Rx-Ry ' + 'on velocity equal ' + str(V[maxis[0][i]]) + 'm/s' )
#            plt.xlabel('range Y')
#            plt.ylabel('range X')
#            plt.ylim([0,150])
    

#    frame_num = str(Z) 
#    dir_name_locat_out = 'D:\\work\\locat\\real_time_model\\real_time_model\\locat_imgs\\'
#    route_out_locat = dir_name_locat_out + 'locat_img_' + frame_num.rjust(5, '0') + '.png'
#    plt.savefig(route_out_locat, dpi=240)
    
#    plt.plot([1,2,3])
    
#    plt.figure(1)
#    plt.subplot(131)
##    plt.figure(1)
##    plt.pcolormesh(teth_axe0, d_axe_new, res0.T)
##    plt.colorbar()
##    plt.grid('on')
##    plt.title('2D FFT ESTIMATE R-A')
##    plt.xlabel('azimuth')
##    plt.ylabel('range')  
#    X, Y = np.meshgrid(d_axe_new, teth_axe0)
#    plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), res0, cmap = cmap)
#    plt.colorbar()
#    plt.title('2D FFT ESTIMATE Rx-Ry')
#    plt.xlabel('range Y')
#    plt.ylabel('range X') 
#    plt.ylim([0,100])
#    
#    res1 = res0
#
#    for i in range(max_d_num):
#        for k in range(resiv):
#            res1[k,i] = res1[k,i]*koef*i*i
#    
#    plt.subplot(132)
##    plt.figure(2)
##    plt.pcolormesh(teth_axe0, d_axe_new, res0.T)
##    plt.colorbar()
##    plt.grid('on')
##    plt.title('2D FFT ESTIMATE R-A')
##    plt.xlabel('azimuth')
##    plt.ylabel('range')  
#    X, Y = np.meshgrid(d_axe_new, teth_axe0)
#    plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), res1, cmap = cmap)
#    plt.colorbar()
#    plt.title('2D FFT ESTIMATE Rx-Ry')
#    plt.xlabel('range Y')
#    plt.ylabel('range X') 
#    plt.ylim([0,100])
#    
#    res2 = res1
#    
#    for i in range(max_d_num):
#        for k in range(resiv):
#
#            if (res2[k,i] < 0.35*np.amax(res2)):
#                res2[k,i] = 0
#     
#    plt.subplot(133)           
##    plt.figure(3)
##    plt.pcolormesh(teth_axe0, d_axe_new, res0.T)
##    plt.colorbar()
##    plt.grid('on')
##    plt.title('2D FFT ESTIMATE R-A')
##    plt.xlabel('azimuth')
##    plt.ylabel('range')   
#    X, Y = np.meshgrid(d_axe_new, teth_axe0)
#    plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), res2, cmap = cmap)
#    plt.colorbar()
#    plt.title('2D FFT ESTIMATE Rx-Ry')
#    plt.xlabel('range Y')
#    plt.ylabel('range X') 
#    plt.ylim([0,100])
    
    print("now is "+str(Z)+" frame")

print("==============================END===============================")