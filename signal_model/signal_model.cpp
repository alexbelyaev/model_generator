#include "signal_model.h"
#include <ctime>

// ==========================================================================================
//     ������� get_signal_model_sample_c ��������� ���������� �������� ������������ ������� �
//  �������� ������ ������� ��� �������� �������� � �������� � �����, ���������� ��������
//  � ���� ���������� ������������ ����
// ==========================================================================================
/*

������������ ���������:
  Sample_id &sample     - ���������-�������� ������� ������������ �������
  Signal_params &signal - ���������-�������� ���������� ������������ ������� � 
                          ���� (signal_model.h)
  vector<vector<double>> targets - ��������� ������ ���������� ���������������� �����
  int targets_num - ����� �����

������������ ���������:
  complex_s sample_value - �������� ������������ ������� � �������� ������ ������� ��� 
                           �������� �������� � �������� � �����
*/
// ==========================================================================================
// ==========================================================================================
complex_s get_signal_model_sample_c(Sample_id &sample, Signal_params &signal, 
                                    vector<vector<double>> targets, int targets_num) {
  int rx_ref  = sample._rx_ref;
  int ps_ref  = sample._ps_ref;
  int smp_ref = sample._smp_ref;

  // ����������� ��������������� �� ����������� ������� (1/�)
  double k  = signal._k;
  // ���������� ����� ���������� (�)
  double d  = signal._d;
  // �������� ������� ��������  (�)
  double d0 = signal._d0;

  complex_s i = complex_s(0.0, 1.0);
  complex_s sample_value = complex_s(0.0, 0.0);

  // ������ ������� � ��������
  double t_smp = smp_ref*signal._t_d;
  // ������ ������� � ��������
  double t_strb = ps_ref*signal._t_p + t_smp;

  for (int i_mark = 0; i_mark < targets_num; i_mark++) {
    // ��������� ����
    double R       = targets[0][i_mark];
    double v       = targets[1][i_mark];
    double tetha   = targets[2][i_mark]*GRADIAN;
    double rsc_mod = targets[3][i_mark];
    double rsc_arg = targets[4][i_mark]*GRADIAN;

    // ����� ��������������� �� ���� �� ��������
    double t_prop = 2*R/c;
    // ���������� �������
    double f_dif  = signal._S*t_prop;
    // ������������ �������� ������� (�� ����������� �������)
    double f_dop  = 2.0*signal._f_c*v/c;

    // ���� ������������ �������
    double sig_phase = rsc_arg + k*2*R + k*(d0 + rx_ref*d)*sin(tetha);
    sig_phase += 2*pi*(f_dif*t_smp + f_dop*t_strb);
    complex_s exponent_value = complex_s(0, sig_phase);

    if (t_prop > t_smp)
      continue;
    else
      sample_value += signal._A*rsc_mod*exp(exponent_value) / (16*pi_2*R*R);
  }
  return sample_value;
}
// ==========================================================================================



// ==========================================================================================
//     ������� get_signal_model_sample ��������� ���������� �������� ������������ ������� �
//  �������� ������ ������� ��� �������� �������� � �������� � �����, ����������
//  ������������ � ������ ����� �� ���������� ����������
// ==========================================================================================
/*

������������ ���������:
  Sample_x &sample_v    - ��������� ��� �������� �������� ������������ ������� � ��������
                          ������ ������� ��� �������� �������� � �������� � �����
                          ������������ ����� (sample_v.inp), ������ (sample_v.qdr)
  double *sample_image  - ������ ����� ������������ ������� � �������� ������ ������� 
                          ��� �������� �������� � �������� � �����
  Sample_id &sample     - ���������-�������� ������� ������������ �������
  Signal_params &signal - ���������-�������� ���������� ������������ ������� � 
                          ���� (signal_model.h)
  vector<vector<double>> targets - ��������� ������ ���������� ���������������� �����
  int targets_num - ����� �����

������������ ���������:
  status
    0 - ������� ���������� �������;
    1 - ������ ������������� ��������� ����������;

*/
// ==========================================================================================
// ==========================================================================================
int get_signal_model_sample(Sample_x &sample_v, Sample_id &sample, Signal_params &signal, 
                            vector<vector<double>> targets, int targets_num)
{
  sample_v.inp = 0.0;
  sample_v.qdr = 0.0;
  int rx_ref  = sample._rx_ref;
  int ps_ref  = sample._ps_ref;
  int smp_ref = sample._smp_ref;

  // ����������� ��������������� �� ����������� ������� (1/�)
  double k  = signal._k;
  // ���������� ����� ���������� (�)
  double d  = signal._d;
  // �������� ������� ��������  (�)
  double d0 = signal._d0;

  // ������ ������� � ��������
  double t_smp = smp_ref*signal._t_d;
  // ������ ������� � ��������
  double t_strb = ps_ref*signal._t_p + t_smp;

  for (int i_mark = 0; i_mark < targets_num; i_mark++) {
    // ��������� ����
    double R       = targets[0][i_mark];
    double v       = targets[2][i_mark];
    double tetha   = targets[1][i_mark]*GRADIAN;
    double rsc_mod = targets[3][i_mark];
    double rsc_arg = targets[4][i_mark]*GRADIAN;

    // ����� ��������������� �� ���� �� ��������
    double t_prop = 2*R/c;
    // ���������� �������
    double f_dif  = signal._S*t_prop;
    // ������������ �������� ������� (�� ����������� �������)
    double f_dop  = 2.0*signal._f_c*v/c;

    // ���� ������������ �������
    double sig_phase = rsc_arg + k*2*R + k*(d0 + rx_ref*d)*sin(tetha);
    sig_phase += 2*pi*(f_dif*t_smp + f_dop*t_strb);


    if (t_prop > t_smp)
      continue;
    else {
      double amplitude = signal._A*rsc_mod / (16*pi_2*R*R);
      sample_v.inp += amplitude*cos(sig_phase);
      sample_v.qdr += amplitude*sin(sig_phase);
    }
  }
  return 0;
}
// ==========================================================================================



// ==========================================================================================
//     ������� get_signal_model ��������� ���������� �������� ������������ ������� �� ���
//  ������� ������� ��� ���� ��������� � ��������� � �����, ����������
//  ������������ � ������ ����� �� ���������� ����������
// ==========================================================================================
/*

������������ ���������:
  float *inp - ��������� �� ������ ��� �������� �������� ��������� ������������ 
               ������������ �������;
  float *qdr - ��������� �� ������ ��� �������� �������� ������������ ������������ 
               ������������ �������;
               ������� �������� ������ ����: sizeof(double)*rx_num*ps_num*smp_num;
               ������ �������� �������:
               ��� ������� ��������, ��� ������� ��������, ������ ������ �� �����:
                0 �������:
                  0 �������:
                    0 ������, 1 ������, 2 ������, ... (smp_num-1) ������;
                  ...
                  (ps_num-1) �������:
                    0 ������, 1 ������, 2 ������, ... (smp_num-1) ������;
                ...
                (rx_num-1) �������: ...
  Sample_id &sample     - ���������-�������� ������� ������������ �������
  Signal_params &signal - ���������-�������� ���������� ������������ ������� � 
                          ���� (signal_model.h)
  vector<vector<double>> targets - ��������� ������ ���������� ���������������� �����
  int targets_num - ����� �����

������������ ���������:
  status
    0 - ������� ���������� �������;
    1 - �������� ������� ���������;
    2 - ������������������ ��������� ������ �������

*/
// ==========================================================================================
// ==========================================================================================
int get_signal_model(float* inp, float* qdr, 
                     Signal_params &signal, 
                     vector<vector<double>> targets, int targets_num)
{
  if (inp == NULL || qdr == NULL)
    return 1;
  if (signal._init != 2)
    return 2;

  // ����������� ��������������� �� ����������� ������� (1/�)
  double k  = signal._k;
  // ���������� ����� ���������� (�)
  double d  = signal._d;
  // �������� ������� ��������  (�)
  double d0 = signal._d0;

  for (register int rx_ref = 0; rx_ref < signal._rx_num; rx_ref++)
  {
    // ������������� ����� �������� rx_ref �������� �� ������ ���������
    double rx_shift = k*(d0 + rx_ref*d);
    for (register int ps_ref = 0; ps_ref < signal._ps_num; ps_ref++)
    {
      // ������������ �� ������� ��� ps_ref �������� ������������ ������ �������
      double ps_shift = ps_ref*signal._t_p;
      for (register int smp_ref = 0; smp_ref < signal._smp_num; smp_ref++)
      {
        // ������ ������� � ��������
        double t_smp = smp_ref*signal._t_d;
        // ������ ������� � ��������
        double t_strb = ps_shift + t_smp;

        // �������� �������� ��������� � ������������ ������������
        float inp_ = 0.0;
        float qdr_ = 0.0;

        // ��� ������ ����
        for (register int i_mark = 0; i_mark < targets_num; i_mark++) 
        {
          // ��������� ����
          double R       = targets[0][i_mark];
          double v       = targets[1][i_mark];
          double tetha   = targets[2][i_mark]*GRADIAN;
          double rsc_mod = targets[3][i_mark];
          double rsc_arg = targets[4][i_mark]*GRADIAN;

          // ����� ��������������� �� ���� �� ��������
          double t_prop = 2*R/c;
          // ���������� �������
          double f_dif  = signal._S*t_prop;
          // ������������ �������� ������� (�� ����������� �������)
          double f_dop  = 2.0*signal._f_c*v/c;

          // ���� ������������ �������
          double sig_phase = rsc_arg + k*2*R + rx_shift*sin(tetha);
          sig_phase += 2*pi*(f_dif*t_smp + f_dop*t_strb);

          if (t_prop > t_smp)
            continue;
          else {
            double amplitude = signal._A*rsc_mod / (16*pi_2*R*R);
            inp_ += amplitude*cos(sig_phase);
            qdr_ += amplitude*sin(sig_phase);
          }
        }
        // ������ �������� ������������ ������� � ����� ���������� � ��������� ������
		    *inp++ = (float)inp_;
        *qdr++ = (float)qdr_;
      }
    }
  }

  return 0;
}
// ==========================================================================================

// ==========================================================================================
//  ������� get_signal_n_model ��������� ���������� �������� ������������ ������� � �����
//	�� ��� ������� ������� ��� ���� ��������� � ��������� � �����, ����������
//  ������������ � ������ ����� �� ���������� ����������
// ==========================================================================================
/*

������������ ���������:
  float *inp - ��������� �� ������ ��� �������� �������� ��������� ������������ 
               ������������ �������;
  float *qdr - ��������� �� ������ ��� �������� �������� ������������ ������������ 
               ������������ �������;
               ������� �������� ������ ����: sizeof(double)*rx_num*ps_num*smp_num;
               ������ �������� �������:
               ��� ������� ��������, ��� ������� ��������, ������ ������ �� �����:
                0 �������:
                  0 �������:
                    0 ������, 1 ������, 2 ������, ... (smp_num-1) ������;
                  ...
                  (ps_num-1) �������:
                    0 ������, 1 ������, 2 ������, ... (smp_num-1) ������;
                ...
                (rx_num-1) �������: ...
  snr_params &snr       - ���������-�������� ���������� ���������� ����� ������� � ����
  Sample_id &sample     - ���������-�������� ������� ������������ �������
  Signal_params &signal - ���������-�������� ���������� ������������ ������� � 
                          ���� (signal_model.h)
  vector<vector<double>> targets - ��������� ������ ���������� ���������������� �����
  int targets_num - ����� �����

������������ ���������:
  status
    0 - ������� ���������� �������;
    1 - �������� ������� ���������;
    2 - ������������������ ��������� ������ �������

*/
// ==========================================================================================
// ==========================================================================================
int get_signal_n_model(float* inp, float* qdr, snr_params &snr,
                     Signal_params &signal, 
                     vector<vector<double>> targets, int targets_num)
{
  if (inp == NULL || qdr == NULL)
    return 1;
  if (signal._init != 2)
    return 2;

  // ����������� ��������������� �� ����������� ������� (1/�)
  double k  = signal._k;
  // ���������� ����� ���������� (�)
  double d  = signal._d;
  // �������� ������� ��������  (�)
  double d0 = signal._d0;
  // ��� �������� ���� (�)
  double mean = 0;
  // ����������� ���������� ���� (�)
  double std = signal._std;
  // ��������������� ���������� ��� ������� ��������� ������/���
  double noise_real = 0;
  double noise_imag = 0;
   
  double tmp_n_real = 0;
  double tmp_n_imag = 0;

  double tmp_s_real = 0;
  double tmp_s_imag = 0;

  double r_n = 0;
  double r_s = 0;

  double n = signal._rx_num * signal._ps_num * signal._smp_num;;
  double phi_n = 0;
  double phi_s = 0;

  float SNR_inphase = 0;
  float SNR_quadrature = 0;
  //************************************************************

  srand(time(NULL)); // ��������� �������� ���������� ��������� �����

  for (register int rx_ref = 0; rx_ref < signal._rx_num; rx_ref++)
  {
    // ������������� ����� �������� rx_ref �������� �� ������ ���������
    double rx_shift = k*(d0 + rx_ref*d);
    for (register int ps_ref = 0; ps_ref < signal._ps_num; ps_ref++)
    {
      // ������������ �� ������� ��� ps_ref �������� ������������ ������ �������
      double ps_shift = ps_ref*signal._t_p;
      for (register int smp_ref = 0; smp_ref < signal._smp_num; smp_ref++)
      {
        // ������ ������� � ��������
        double t_smp = smp_ref*signal._t_d;
        // ������ ������� � ��������
        double t_strb = ps_shift + t_smp;

        // �������� �������� ��������� � ������������ ������������
        float inp_ = 0.0;
        float qdr_ = 0.0;

        // ��� ������ ����
        for (register int i_mark = 0; i_mark < targets_num; i_mark++) 
        {
          // ��������� ����
          double R       = targets[0][i_mark];
          double v       = targets[1][i_mark];
          double tetha   = targets[2][i_mark]*GRADIAN;
          double rsc_mod = targets[3][i_mark];
          double rsc_arg = targets[4][i_mark]*GRADIAN;

          // ����� ��������������� �� ���� �� ��������
          double t_prop = 2*R/c;
          // ���������� �������
          double f_dif  = signal._S*t_prop;
          // ������������ �������� ������� (�� ����������� �������)
          double f_dop  = 2.0*signal._f_c*v/c;

          // ���� ������������ �������
          double sig_phase = rsc_arg + k*2*R + rx_shift*sin(tetha);
          sig_phase += 2*pi*(f_dif*t_smp + f_dop*t_strb);

          if (t_prop > t_smp)
            continue;
          else {
            double amplitude = signal._A*rsc_mod / (16*pi_2*R*R);
            inp_ += amplitude*cos(sig_phase);
            qdr_ += amplitude*sin(sig_phase);
          }
        }
    // ������ �������� ������������ ������� � ����. ����� ���������� � ��������� ������
		noise_real = rand_normal(mean, std); 
		noise_imag = rand_normal(mean, std);

    *inp++ = (float)inp_ + noise_real;
    *qdr++ = (float)qdr_ + noise_imag;


		// ������ �������� ������������ ������� � ���� ��� ����������� ������� RMS

		tmp_n_real += (noise_real * noise_real);
		tmp_n_imag += (noise_imag * noise_imag);

		tmp_s_real += (inp_ * inp_);
		tmp_s_imag += (qdr_ * qdr_);

      }
    }
  }
  // ������ RMS � ��������� ������ ��� � �� 

  snr.RMS_n_real = (float)sqrt(tmp_n_real/n);
  snr.RMS_n_imag = (float)sqrt(tmp_n_imag/n);

  snr.RMS_s_real = (float)sqrt(tmp_s_real/n);
  snr.RMS_s_imag = (float)sqrt(tmp_s_imag/n);

  //cout<<"RMS noise inphase    = "<<RMS_n_real<<" V \n";
  //cout<<"RMS noise quadrature = "<<RMS_n_imag<<" V \n";
  //cout<<"RMS signal inphase   = "<<RMS_s_real<<" V \n";
  //cout<<"RMS signal quadrature = "<<RMS_s_imag<<" V \n";

  snr.SNR_inp = (20 * log10(snr.RMS_s_real/snr.RMS_n_real));
  snr.SNR_qdr = (20 * log10(snr.RMS_s_imag/snr.RMS_n_imag));

  return 0;
}
// ==========================================================================================



// ==========================================================================================
//     ������� init_Signal_params ��������� ������������� ��������� ����������
//  ���������-�������� ������������ ������� � ����
// ==========================================================================================
/*

������������ ���������:
  Signal_params &signal - ���������-�������� ���������� ������������ ������� � 
                          ���� (signal_model.h)
������������ ���������:
  status
    0 - ������� ���������� �������;
    1 - ������ ������������� ��������� ����������;
*/
// ==========================================================================================
// ==========================================================================================
int init_Signal_params(Signal_params &signal) {
  if (signal._init) {
    signal._f_p = 1/signal._t_p;
    signal._f_hi = signal._f0 + signal._t_m*signal._S;
    signal._f_c = (signal._f0 + signal._f_hi)/2.0;
    signal._w_ln = c/signal._f_c;
    signal._k = 2*pi/signal._w_ln;
    signal._t_d = 1/signal._f_d;
    signal._smp_num = (int)(signal._t_m*signal._f_d);
    signal._init = 2;
    return 0;
  }
  else
    return 1;
}
// ==========================================================================================




// ==========================================================================================
//     ������� targets_reader ��������� ������ ���������� ���������������� ����� �
//  �� ������ � ��������� ��������� vector
// ==========================================================================================
/*
// ==========================================================================================
������ �������� txt �����:
#   R, m      v, m/s   tetha, grad   rcs_mod   rcs_arg, grad # target number
    24.1        1.0        0.0         1.0         0.0       # 1
    34.0        2.0        10.0        1.0         0.0       # 2
    44.0        3.0        20.0        1.0         0.0       # 3
// ==========================================================================================

������������ ������:
#include <iostream>
#include <fstream>

������������ ���������:
  vector<vector<double>> trg_vec - ������ ��������� ������ �������� 5 * 1
	  trg_vec[0]  // ������ ��� �������� ���������� (�)
	  trg_vec[1] 	// ������ ��� �������� ��������� (�/�)
	  trg_vec[2]  // ������ ��� �������� �������� (�������)
	  trg_vec[3] 	// ������ ��� �������� ������ ����������� ���������
	  trg_vec[4]  // ������ ��� �������� ���� ����������� ��������� (�������)
  string path - ���� � ���������� ����� � ����������� �����

������������ ���������:
  status
    0 - ������� ���������� �������;
    1 - ������ ������ �����;
*/
// ==========================================================================================
// ==========================================================================================
int targets_reader(vector<vector<double>> &trg_vec, string path)
{

	ifstream myReadFile;
	string s;
	int value_begin = 0; // ���������� �������� ��������� �������� � ������
	int value_end = 0;   // ��������� �������� ��������� �������� � ������
	int value_cnt = 0;   // �������� ��������� �������� � ������
	bool value_save = false; // ������� �� ���������� ��������� �������� � ������

	myReadFile.open(path); // ������ �� �����

	if (myReadFile.is_open() == false) // ���� ���� �� ������
		return 1;

	if (myReadFile.is_open()) // ���� ���� ������
	{
		while (getline(myReadFile, s)) // ����������� ���������� ����� �� �����
		{
			for (int i = 0; i < s.length(); i++) // ������������ ������������ ������� ������
			{
				if (s[i] == '#') // ���� ������ ���� ������, �� ��������� ����
					break; // ����� �� �����

				if (i == 0) // �������� ��� ������� ������ ���������� � ��������� ��������
				{
					if (s[i] != ' ') 
						value_begin = i; // ���������� ������� �������� ������ ��������� ��������
				}

				if (i != 0) // �������� ��� ������� ������ ���������� � �������
				{
					if ((s[i] != ' ') & (s[i-1] == ' ')) // ������� ���������� ������� �������� ������ ��������� ��������
  					value_begin = i; // ���������� ������� �������� ������ ��������� ��������
				}

				if ((i != s.length()) & (i != 0)) // ���������� �������� ��������� � ��������������� �������� �������
				{
					if ((s[i] == ' ') & (s[i-1] != ' ')) // ������� ���������� ���������� �������� ������ ��������� ��������
					{
						value_end = i - 1; // ���������� ���������� �������� ������ ��������� ��������
						value_save = true; // ������� �� ������ ��������� ��������
					}
				}
				
				if (value_save == true) // ������� ��� ������� ���������� ������ �������� ��������� ��������
				{
					value_save = false; // ����� ������� �� ������ ��������� ��������
					
					if (value_cnt == 0) // ������� ��� ������ ������� ������� (���������)
					{
						string s_tmp = s.substr(value_begin, value_end); // ���������� ����� ������ � ������� �������� ���������
						double tmp = atof(s_tmp.c_str()); // ������� ��������� �������� �� string �  double
						trg_vec[0].insert(trg_vec[0].end(), tmp); // ���������� ������ �������� � ������
					}

					if (value_cnt == 1) // ������� ��� ������ ������� ������� (��������)
					{
						string s_tmp = s.substr(value_begin, value_end); // ���������� ����� ������ � ������� �������� ���������
						double tmp = atof(s_tmp.c_str()); // ������� ��������� �������� �� string �  double
						trg_vec[1].insert(trg_vec[1].end(), tmp); // ���������� ������ �������� � ������
					}

					if (value_cnt == 2) // ������� ��� ������ �������� ������� (�������)
					{
						string s_tmp = s.substr(value_begin, value_end); // ���������� ����� ������ � ������� �������� ���������
						double tmp = atof(s_tmp.c_str()); // ������� ��������� �������� �� string �  double
						trg_vec[2].insert(trg_vec[2].end(), tmp); // ���������� ������ �������� � ������
					}

					if (value_cnt == 3) // ������� ��� ������ ������ ������� (������ ����������� ���������)
					{
						string s_tmp = s.substr(value_begin, value_end); // ���������� ����� ������ � ������� �������� ���������
						double tmp = atof(s_tmp.c_str()); // ������� ��������� �������� �� string �  double
						trg_vec[3].insert(trg_vec[3].end(), tmp); // ���������� ������ �������� � ������
					}

					if (value_cnt == 4) // ������� ��� ������ ������� ������� (���� ����������� ���������)
					{
						string s_tmp = s.substr(value_begin, value_end); // ���������� ����� ������ � ������� �������� ���������
						double tmp = atof(s_tmp.c_str()); // ������� ��������� �������� �� string �  double
						trg_vec[4].insert(trg_vec[4].end(), tmp); // ���������� ������ �������� � ������
					}
					value_cnt += 1; // ���������� �� 1 �������� �������� �������� � ������
					value_begin = 0; // ��������� ������� �������� ������ ��������� ��������
					value_end = 0; // ��������� ���������� �������� ������ ��������� ��������
				}
			}
			value_cnt = 0; // ��������� �������� �������� �������� � ������
		}
	}

	myReadFile.close(); // �������� �����

	return 0;
}
// ==========================================================================================



// ==========================================================================================
//     ������� params_reader ��������� ������ ���������� ������ FMCW ������ �� ����� � 
//  �� ������ � ���������-��������
// ==========================================================================================
/*
// ==========================================================================================
������ �������� txt �����:
77e+9     # ������ ������� � ������� ������������ ������� (��)
1e+13     # �������� ���������� ������� (��/�) 1e-12 (���/���)
1e-4      # ����� ������ ��� (�������� ������� ���������� �������) (�)
2e-4      # ������������ ������������ �������� (�)
128       # ����� ��������� � �����
1e0       # ��������� ��������� (�/�)
32        # ����� ���������
1.948e-3  # ���������� ����� ���������� (�)
9.74e-3   # �������� ������� ��������  (�)
15e6      # ������� ������������� (��)
0.1       # ��� ���� (�)
// ==========================================================================================

������������ ������:
#include <iostream>
#include <fstream>

������������ ���������:
  Signal_params &param_struct - ���������-�������� ���������� ������������ ������� � 
                                ���� ��������� (signal_model.h)
  string path - ���� � ���������� ����� � ����������� ������ ������� � ����

������������ ���������:
  status
    0 - ������� ���������� �������;
    1 - ������ ������ �����;
*/
// ==========================================================================================
// ==========================================================================================
int params_reader(Signal_params &param_struct, string path)
{
	ifstream myReadFile;
	string s;
	int value_begin = 0; // ����� ������� ������� ������� (�������) ��������� �������� � ������
	int value_end = 0;   // ����� ������� ���������� ������� ��������� �������� � ������
	int value_cnt = 0;   // ������� �������� �������� � ������
	int str_cnt = 0;     //  ����� ������� ������ 
	bool value_save = false; // ������� �� ���������� ��������� ��������

  myReadFile.open(path);
	if (myReadFile.is_open() == false) // ���� ���� �� ������
		return 1;

	if (myReadFile.is_open())
	{
		while (getline(myReadFile, s))
		{
			for (int i = 0; i < s.length(); i++)
			{
				if (s[i] == '#')
					break;

        // ������ ���������� � ��������� ��������
				if (i == 0) 
				{
					if (s[0] != ' ') 
						value_begin = 0; // ���������� ������� �������� ������ ��������� ��������
				}
        // ������ ���������� � �������
        else if ((s[i] != ' ') & (s[i-1] == ' '))
          value_begin = i; // ���������� ������� �������� ������ ��������� ��������

				if ((i != s.length()) & (i != 0)) // ���������� �������� ��������� � ��������������� �������� �������
				{
					if ((s[i] == ' ') & (s[i-1] != ' ')) // ������� ���������� ���������� �������� ������ ��������� ��������
					{
						value_end = i; // ���������� ���������� �������� ������ ��������� ��������
						value_save = true; // ������� �� ������ ��������� ��������
					}
				}

				if (value_save)
				{
					value_save = false;
					if (value_cnt == 0)
					{
            // ������ ����� ������ �� ���������
						string s_tmp = s.substr(value_begin, value_end);
						double tmp = atof(s_tmp.c_str());

						// ���������� ��������� ������
						if (str_cnt == 0)
							param_struct._f0 = tmp;
						else if (str_cnt == 1)
							param_struct._S = tmp;
						else if (str_cnt == 2)
							param_struct._t_m = tmp;
						else if (str_cnt == 3)
							param_struct._t_p = tmp;
						else if (str_cnt == 4)
							param_struct._ps_num = int(tmp); // �������������� double � int
						else if (str_cnt == 5)
							param_struct._A = tmp;
						else if (str_cnt == 6)
							param_struct._rx_num = int(tmp); // �������������� double � int
						else if (str_cnt == 7)
							param_struct._d = tmp;
						else if (str_cnt == 8)
							param_struct._d0 = tmp;
						else if (str_cnt == 9)
							param_struct._f_d = tmp;
						else if (str_cnt == 10)
							param_struct._std = tmp; 
					}
					value_cnt++;  // ���������� �� 1 �������� �������� �������� � ������
					value_begin = 0; // ��������� ������� �������� ������ ��������� ��������
					value_end = 0;   // ��������� ���������� �������� ������ ��������� ��������
				}
			}

			value_cnt = 0; // ��������� �������� �������� �������� � ������
			str_cnt += 1; // ���������� �� 1 �������� ������ ������
		}
	}

	myReadFile.close(); // �������� �����
	
	return 0;
}
// ==========================================================================================

/*
########################################################
## ������� ��������� ���� � ���������� �������������� ##
########################################################

������������ ������:

������� ���������:

mean - ��� ��������
stddev - �������������������� ����������

//*******************************************************

�������� ���������:
double - ��������� �����

*/

double rand_normal(double mean, double stddev)
{
	//����� �����-�������
	static double n2 = 0.0;
    static int n2_cached = 0;
    if (!n2_cached)
    {
        double x, y, r;
        do
        {
			
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);
        {
            double d1 = sqrt(-2.0*log(r)/r);
            double n1 = x*d1;
            n2 = y*d1;
            double result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else
    {
        n2_cached = 0;
        return n2*stddev + mean;
    }
}
