#ifndef basic_processing


#include <iostream>
#include <string>
#include <fstream>

#include <iomanip>
#include <cmath>
#include <ctime>

using namespace std;


#define  fft_transform_sour 
#define  NUMBER_IS_2_POW_K(x)   ((!((x)&((x)-1)))&&((x)>1))
#define	 POW2(x)				(x*x)
#define  FT_DIRECT			   -1    
#define  FT_INVERSE			    1
#define  CPY_DIRECT			    true    
#define  CPY_INVERSE		    false
#define  Pi						3.1415926535897932384626433832795
#define  TwoPi					6.283185307179586
#define  grad_rad				0.01745329251994329576923690768489
#define  c_const				299792458
#define  fc						77e9
#define  lamb					(c_const/ fc)
#define  d						lamb / 2
	
double radians(double);
inline void mascpy(float*, float*, int);
inline void mascpy(float**, float*, int,int);
inline void mascpy(float*, float**, int, int);
void mascpy(float**, float**, int, int, int, bool);

bool FFT(float*, float*, int, int);
void fft_2d(float**, float**, int, int);
void fft_3d(float**, float**, int, int, int);
void fft_3d_corr(float**, float**, int, int, int);
bool fftshift(float*, float*, int);
void DOA(float* , float*, int, float, float, int);

void buffer_3d(float*,float**,int,int,int,int,int);

bool save_npy(float*, float*, int, string);
bool save_npy_2d(float**, float**, int, int, string);

#endif







/*http://atlassian-srv:8080*/