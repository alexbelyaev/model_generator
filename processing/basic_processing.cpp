#include "basic_processing.h"



/*������� �� �������� � ������� ��� ����������� � ��������� ������������������ �������*/
double radians(double grad){
	double radian = grad*Pi/180;
	return radian;
}

//////////////////////////////////////////////////////////////////////////
/*��������������� ������� ��� ���������� ���������� �������������� �����*/
/***********************************BEGIN********************************/

/*1. ������� ����������� i-�� ������ ���������� �������� ������� � ��������� ����� ��� ������� ����������� FFT*/
/*
������� ���������:
	*copy  - ��������� �� ������ ���������� �����, ��� float;
	*paste - ��������� �� ������ ������, ��� float; 
	len    - ����� ������� ������������������, ��� int;
	(PS ������� ��������� ������������ ����������� � � �������� �����������, �.�. �� ������ � ������)
*/
inline void mascpy(float *copy, float *paste, int len){
	for (register int i = 0; i < len; i++){
		*(paste++)   = *(copy++);
		/**(paste+i+1) = *(copy+i+1);
		*(paste+i+2) = *(copy+i+2);
		*(paste+i+3) = *(copy+i+3);
		*(paste+i+4) = *(copy+i+4);*/
	}
		
}

/*2. ������� ����������� i-��� ������� ���������� �������� ������� � ��������� ����� ��� ������� ����������� FFT*/
/*
������� ���������:
	**copy   - ��������� ��������� �� ������ i-��� ����������� ������� ���������� �������, ��� float;
	*paste   - ��������� �� ������ ������, ��� float; 
	len      - ����� ������� ������������������, ��� int;
	numofcol - ����� ����������� i-��� ������� ���������� �������;
*/
inline void mascpy(float **copy, float *paste, int len,int numofcol){
	for (register int i = 0; i < len; i++){
		*(paste+i) = *(*(copy + i)+numofcol);
	}
}

/*3. ������� ������� ����������� FFT � i-�� ������ ���������� �������*/
/*��������� ������� 1.*/

/*4. ������� ����������� ����������� FFT � i-�� ������� ���������� �������*/
/*
������� ���������:
	*copy    - ��������� �� ������ ������, ��� float;
	*paste   - ��������� ��������� �� ������ i-��� ������� ���������� �������, ��� float; 
	len      - ����� ������� ������������������, ��� int;
	numofcol - ����� ����������� i-��� ������� ���������� �������;
*/
inline void mascpy(float *copy, float **paste, int len,int numofcol){
	for (register int i = 0; i < len; i++){
		*(*(paste + i)+numofcol) = *(copy+i);
	}
}

/************************************END*********************************/
/*��������������� ������� ��� ���������� ���������� �������������� �����*/
//////////////////////////////////////////////////////////////////////////

/*���������� ����� ��������������*/
/*
������� ���������:
	*Rdat   - ��������� �� ������ �������������� ������ ����������� �����, ��� float;
	*Idat   - ��������� �� ������ ������ ������ ����������� �����, ��� float; 
	N       - ����� ������� ������������������, ��� int;
	LogN    - �������� ��������� �� N �� ��������� 2, ��� int
	Ft_Flag - ���������� ���� �������������� �����, ��������, ������ FT_DIRECT ��� ������� ��������������,
	��������, ������ FT_INVERSE ��� ��������� ��������������.
*/
bool FFT(float *Rdat, float *Idat, int N, int Ft_Flag){
	int LogN = log(N)/log(2);
  // parameters error check:
  if((Rdat == NULL) || (Idat == NULL))                  return false;
  if((N > 16384) || (N < 1))                            return false;
  if(!NUMBER_IS_2_POW_K(N))                             return false;
  if((LogN < 2) || (LogN > 14))                         return false;
  if((Ft_Flag != FT_DIRECT) && (Ft_Flag != FT_INVERSE)) return false;

  register int  i, j, n, k, io, ie, in, nn;
  float         ru, iu, rtp, itp, rtq, itq, rw, iw, sr;
  
  static const float Rcoef[14] =
  {  -1.0000000000000000F,  0.0000000000000000F,  0.7071067811865475F,
      0.9238795325112867F,  0.9807852804032304F,  0.9951847266721969F,
      0.9987954562051724F,  0.9996988186962042F,  0.9999247018391445F,
      0.9999811752826011F,  0.9999952938095761F,  0.9999988234517018F,
      0.9999997058628822F,  0.9999999264657178F
  };
  static const float Icoef[14] =
  {   0.0000000000000000F, -1.0000000000000000F, -0.7071067811865474F,
     -0.3826834323650897F, -0.1950903220161282F, -0.0980171403295606F,
     -0.0490676743274180F, -0.0245412285229122F, -0.0122715382857199F,
     -0.0061358846491544F, -0.0030679567629659F, -0.0015339801862847F,
     -0.0007669903187427F, -0.0003834951875714F
  };
  
  nn = N >> 1;
  ie = N;
  for(n=1; n<=LogN; n++){
    rw = *(Rcoef + LogN - n);
    iw = *(Icoef + LogN - n);
    if(Ft_Flag == FT_INVERSE) iw = -iw;
    in = ie >> 1;
    ru = 1.0F;
    iu = 0.0F;
    for(j=0; j<in; j++){
      for(i=j; i<N; i+=ie){
        io       = i + in;
        rtp      = *(Rdat+i)  + *(Rdat+io);
        itp      = *(Idat+i)  + *(Idat+io);
        rtq      = *(Rdat+i)  - *(Rdat+io);
        itq      = *(Idat+i)  - *(Idat+io);
        *(Rdat+io) = rtq * ru - itq * iu;
        *(Idat+io) = itq * ru + rtq * iu;
        *(Rdat+i)  = rtp;
        *(Idat+i)  = itp;
      }

      sr = ru;
      ru = ru * rw - iu * iw;
      iu = iu * rw + sr * iw;
    }

    ie >>= 1;
  }

  for(j=i=1; i<N; i++){
    if(i < j){
      io       = i - 1;
      in       = j - 1;
      rtp      = *(Rdat+in);
      itp      = *(Idat+in);
      *(Rdat+in) = *(Rdat+io);
      *(Idat+in) = *(Idat+io);
      *(Rdat+io) = rtp;
      *(Idat+io) = itp;
    }

    k = nn;

    while(k < j){
      j   = j - k;
      k >>= 1;
    }

    j = j + k;
  }

  if(Ft_Flag == FT_DIRECT) return true;

  rw = 1.0F / N;

  for(i=0; i<N; i++){
    Rdat[i] *= rw;
    Idat[i] *= rw;
  }

  return true;
}


//////////////////////////////////////////////////////////////////////////
/*��������������� ������� ��� ���������� ����������� �������������� �����*/
/***********************************BEGIN********************************/

/*1. ��������� ����� ��������������*/
/*
������� ���������:
	**in_real - ��������� �� ��������� ������ �������������� ������ ����������� �����, ��� float;
	**in_imag - ��������� �� ������ ������ ������ ����������� �����, ��� float; 
	len_A     - ���������� ����� �������� ���������� �������;
	len_B     - ���������� �������� �������� ���������� �������;
*/
void fft_2d(float **in_real, float **in_imag, int len_A, int len_B){
	float *real_part = new float [len_B];
	float *imag_part = new float [len_B];
	///1d-FFT �� �������
	for (register int i = 0; i < len_A; i++){
		mascpy(*(in_real+i),real_part,len_B);
		mascpy(*(in_imag+i),imag_part,len_B);
		FFT(real_part,imag_part,len_B,FT_DIRECT);
		mascpy(real_part,*(in_real+i),len_B);
		mascpy(imag_part,*(in_imag+i),len_B);
	}
	delete[] real_part, imag_part;

	float *real_part_col = new float [len_A];
	float *imag_part_col = new float [len_A];
	///1d-FFT �� ��������
	for (register int i = 0; i < len_B; i++){
		mascpy(in_real,real_part_col,len_A,i);
		mascpy(in_imag,imag_part_col,len_A,i);
		FFT(real_part_col,imag_part_col,len_A,FT_DIRECT);
		mascpy(real_part_col,in_real,len_A,i);
		mascpy(imag_part_col,in_imag,len_A,i);
	}
	delete[] real_part_col, imag_part_col;
}


/* 2. ������� ����������� i-��� ���� ���������� ������� � ��������� ��������� ����� ��� ������� ���������� FFT � ��������� ��������*/

/*
������� ���������:
��� ������ ����������� ���� � �����, direct = CPYDIRECT:
	**copy     - ��������� ��������� �� ������ i-��� ����������� ����, ��� float;
	**paste    - ��������� ��������� �� ��������� �����, ��� float; 
	len_A      - ���������� ����� �������� ���������� �������, ��� int;
	len_B      - ���������� �������� �������� ���������� �������, ��� int;
	numoffield - ����� ����������� i-��� ���� ���������� �������, ��� int;

��� ������ ������� ������ � ����, direct = CPYINVERSE:
	**copy     - ��������� ��������� �� ���������� ��������� �����, ��� float; t;
	**past e   - ��������� ��������� �� ������ i-��� ����, ��� float; 
	len_A      - ���������� ����� �������� ���������� �������, ��� int;
	len_B      - ���������� �������� �������� ���������� �������, ��� int;
	numoffield - ����� i-��� ���� ���������� �������, � ������� ����� �������� ������ �� ������, ��� int;
*/

void mascpy(float **copy, float **paste, int len_A, int len_B, int numoffield,bool direct){
	//����������� ���� � �����:
	if ( direct){
		for (register int i = 0; i<len_A; i++){
			for (register int j = 0; j<len_B; j++){
				float *copy_ptr = *(copy + numoffield);
				float* paste_ptr = *(paste + i);
				*(paste_ptr + j) = *(copy_ptr + i*len_B + j);
			}
		}
	}

	//������� ������ � ����:
	if (!direct){
		for (register int i = 0; i<len_A; i++){
			for (register int j = 0; j<len_B; j++){
				float *copy_ptr = *(copy + i);
				float* paste_ptr = *(paste + numoffield);
				*(paste_ptr + i*len_B + j) = *(copy_ptr + j);
			}
		}
	}
}

/* 3. ������� ����������� i,j-��� �������� ������� ���� ���������� ������� � ��������� ����� ��� ������� ����������� FFT */
/*
������� ���������:
��� ������ ����������� ���� � �����, direct = CPYDIRECT:
	**copy  - ��������� ��������� �� ������ k-��� ���� �� �������� ������������� i,j-�� �������, ��� float;
	*buff   - ���������� ��������� �� �����, � ������� ���������� i,j-�� �������, ��� float; 
	i,j     - ���������� ����������� �������� �� k-�� ����, ��� int;
	samples - ����� ������ ����, ��� int;
	len_C   - ���������� �����, ��� ������� ���������� �����������;

��� ������ ������� ������ � ����, direct = CPYINVERSE:
	**copy  - ��������� ��������� �� ������ k-��� ����, � ������� ���������� k-�� �������, ��� float;
	*buff   - ���������� ��������� �� �����, �� �������� ������������� k-�� �������, ��� float; 
	i,j     - ���������� ������������� ������ �������� �� k-�� ����, ��� int;
	samples - ����� ������ ����, ��� int;
	len_C   - ���������� �����, ��� ������� ���������� �����������;
*/

inline void mascpy(float **copy, float *buff, int i, int j, int samples, int len_C, bool direct){
	// ����������� i,j-�� ��������� ������� ���� � 1-������ �����
	if (direct){
		for (register int k = 0; k < len_C; k++){
			*(buff + k)   = *(*(copy + k) + i*samples + j);
		}
	}

	// ������� �������� 1-������� ������ � i,j-�� ������� ������� ���� 
	if (!direct){
		for (register int k = 0; k < len_C; k++){
			*(*(copy + k) + i*samples + j)   =  *(buff + k);
		}
	}

}

/************************************END*********************************/
/*��������������� ������� ��� ���������� ����������� �������������� �����*/
//////////////////////////////////////////////////////////////////////////

/* �Ш������� �������������� �����*/

/*
������� ���������:
	**in_real - ��������� �� ���������� ������ ��������� ����� �������������� �����, ��� float;
	**in_imag - ��������� �� ���������� ������ ��������� ����� ������ �����, ��� float;
	len_A     - ���������� �������� � ��������� ����, ��� int;
	len_B     - ���������� ����� � ��������� ����, ��� int;
	len_C     - ���������� ��������� �����, ��� int;
*/

void fft_3d(float **in_real, float **in_imag, int len_A, int len_B, int len_C){
	int samples  = len_A;
	int pulses   = len_B;
	int resivers = 12;
	int lglenC = log(len_C)/log(2);

	float** real_buff = new float* [pulses];
	for (register int i = 0; i<pulses; i++){
		*(real_buff + i) = new float[samples];
	}
	float** imag_buff = new float* [pulses];
	for (register int i = 0; i<pulses; i++){
		*(imag_buff + i) = new float[samples];
	}

	for (register int k = 0; k < resivers; k++){
		mascpy(in_real,real_buff,pulses,samples,k,CPY_DIRECT);
		mascpy(in_imag,imag_buff,pulses,samples,k,CPY_DIRECT);
		fft_2d(real_buff,imag_buff,pulses,samples);
		mascpy(real_buff,in_real,pulses,samples,k,CPY_INVERSE);
		mascpy(imag_buff,in_imag,pulses,samples,k,CPY_INVERSE);
	}

	float* real_buff_1d = new float [len_C]();
	float* imag_buff_1d = new float [len_C]();

	for (register int i = 0; i < pulses; i++){
		for (register int j = 0; j < samples; j++){
			mascpy(in_real,real_buff_1d,i,j,samples,len_C,CPY_DIRECT);
			mascpy(in_imag,imag_buff_1d,i,j,samples,len_C,CPY_DIRECT);
			FFT(real_buff_1d,imag_buff_1d,len_C,FT_DIRECT);
			mascpy(in_real,real_buff_1d,i,j,samples,len_C,CPY_INVERSE);
			mascpy(in_imag,imag_buff_1d,i,j,samples,len_C,CPY_INVERSE);
		}
	}

	for (int i=0; i<pulses; i++){
			delete []real_buff[i];
	}
	delete []real_buff;

	for (int i=0; i<pulses; i++){
			delete []imag_buff[i];
	}
	delete []imag_buff;

	delete []real_buff_1d;
	delete []imag_buff_1d;
}

void fft_3d_corr(float **in_real, float **in_imag, int len_A, int len_B, int len_C){
	int samples  = len_B;
	int pulses   = len_A;
	int resivers = 12;
	int lglenC = log(len_C)/log(2);

	float** real_buff = new float* [pulses];
	for (register int i = 0; i<pulses; i++){
		*(real_buff + i) = new float[samples];
	}
	float** imag_buff = new float* [pulses];
	for (register int i = 0; i<pulses; i++){
		*(imag_buff + i) = new float[samples];
	}

	for (register int k = 0; k < resivers; k++){
		mascpy(in_real,real_buff,pulses,samples,k,CPY_DIRECT);
		mascpy(in_imag,imag_buff,pulses,samples,k,CPY_DIRECT);
		fft_2d(real_buff,imag_buff,pulses,samples);
		mascpy(real_buff,in_real,pulses,samples,k,CPY_INVERSE);
		mascpy(imag_buff,in_imag,pulses,samples,k,CPY_INVERSE);
	}

	float* real_buff_1d = new float [len_C];
	float* imag_buff_1d = new float [len_C];

	for (register int i = 0; i < pulses; i++){
		for (register int j = 0; j < samples; j++){
			mascpy(in_real,real_buff_1d,i,j,samples,len_C,CPY_DIRECT);
			mascpy(in_imag,imag_buff_1d,i,j,samples,len_C,CPY_DIRECT);
			DOA(real_buff_1d,imag_buff_1d,resivers,180,len_C,len_C);
			mascpy(in_real,real_buff_1d,i,j,samples,len_C,CPY_INVERSE);
			mascpy(in_imag,imag_buff_1d,i,j,samples,len_C,CPY_INVERSE);
		}
	}

	for (int i=0; i<pulses; i++){
			delete []real_buff[i];
	}
	delete []real_buff;

	for (int i=0; i<pulses; i++){
			delete []imag_buff[i];
	}
	delete []imag_buff;

	delete []real_buff_1d;
	delete []imag_buff_1d;
}

/*
#################################################################
## ������� ��� ������� ����������� ������� ����������� ������� ##
#################################################################

������������ ������:
#include <iostream>

������������ �������:
inline void mascpy(float *copy, float *paste, int len);

������� ���������:
*sig_real_0 - ��������� �� ������ �������������� ��������
*sig_imag_0 - ��������� �� ������ ������ ��������
N - ����� ����������
d - ���������� ����� ��������� ����������
lamb - ����� �����
delta_ang - �������� ���������� �������� ��������
samples - ���������� ����� ����������� ��� �������
mas_size - ������ �������� *sig_real_0 � *sig_imag_0

//*******************************************************

�������� ���������:


*/

void DOA(float *sig_real_0, float *sig_imag_0, int N, float delta_ang, float samples, int mas_size)
{
	float M_real = 0; // �������������� ����� ����������� �������
	float M_imag = 0; // ������ ����� ����������� �������
	register int a_mas = 0;
	float delta = (delta_ang / 2 - (-delta_ang / 2)) / (samples - 1); // ������ ���� �������������
	float *sig_real_temp = new float[mas_size]; // ��������� ������ ��� ���������� ������� �������������� ��������
	float *sig_imag_temp = new float[mas_size]; // ��������� ������ ��� ���������� ������� ������ ��������
	float a = (-delta_ang / 2); // ������ �������� �������
	float tmp = ((2 * Pi) / lamb) * d * sin((a * grad_rad) + Pi);
	for (register int i = 0; i < samples; i++)
	{
		a_mas++;
		double sum_real = 0;
		double sum_imag = 0;

		for (register int n = 0; n < N; n+=3)
		{
			M_real = cos(n*tmp); // ������ �������������� ����� ����������� �������
			M_imag = sin(n*tmp); // ������ ������ ����� ����������� �������

			sum_real += ((sig_real_0[n] * M_real) - (sig_imag_0[n] * M_imag)); // ������ �������������� ����� �������������� �������
			sum_imag += ((sig_real_0[n] * M_imag) + (sig_imag_0[n] * M_real)); // ������ ������ ����� �������������� �������
			////////////////////////////////////////////////////////////
			M_real = cos((n+1)*tmp); // ������ �������������� ����� ����������� �������
			M_imag = sin((n+1)*tmp); // ������ ������ ����� ����������� �������

			sum_real += ((sig_real_0[(n+1)] * M_real) - (sig_imag_0[(n+1)] * M_imag)); // ������ �������������� ����� �������������� �������
			sum_imag += ((sig_real_0[(n+1)] * M_imag) + (sig_imag_0[(n+1)] * M_real)); // ������ ������ ����� �������������� �������
			////////////////////////////////////////
			M_real = cos((n+2)*tmp); // ������ �������������� ����� ����������� �������
			M_imag = sin((n+2)*tmp); // ������ ������ ����� ����������� �������

			sum_real += ((sig_real_0[(n+2)] * M_real) - (sig_imag_0[(n+2)] * M_imag)); // ������ �������������� ����� �������������� �������
			sum_imag += ((sig_real_0[(n+2)] * M_imag) + (sig_imag_0[(n+2)] * M_real)); // ������ ������ ����� �������������� �������

		}
		sig_real_temp[a_mas - 1] = sum_real; // ������ �������������� ����� �� ��������� ������
		sig_imag_temp[a_mas - 1] = sum_imag; // ������ ������ ����� �� ��������� ������
		a += delta;
	}
	mascpy(sig_real_temp, sig_real_0, mas_size); // ������� ���������� ������� � ��������
	mascpy(sig_imag_temp, sig_imag_0, mas_size); // ������� ���������� ������� � ��������

	delete []sig_real_temp;
	delete []sig_imag_temp;

}


/*
##########################################################
## ������� ��� �������� ���� ����� �������������� ����� ##
##########################################################

������������ ������:
#include <iostream>

������������ �������:
inline void mascpy(float *copy, float *paste, int len);

������� ���������:
*sig_real_0 - ��������� �� ������ �������������� ��������
*sig_imag_0 - ��������� �� ������ ������ ��������
mas_size - ������ �������� *sig_real_0 � *sig_imag_0

//*******************************************************

�������� ���������:
bool - ������ ���������� �������

*/
bool fftshift(float *sig_real_0, float *sig_imag_0, int mas_size)
{
	if (mas_size < 16) return false;

	float *sig_real_temp = new float[mas_size]; // ��������� ������ ��� ���������� ������� �������������� ��������
	float *sig_imag_temp = new float[mas_size]; // ��������� ������ ��� ���������� ������� ������ ��������

	int i_2 = mas_size;
	for (register int i = 0; i < mas_size/2; i+=8)
	{
		//***********REAL***********
		*(sig_real_temp+i) = *(sig_real_0+i); // ����������� ��������� � ������ �� ��������� ������
		*(sig_real_0+i) = *(sig_real_0+i_2); // ������ ��������� � ������ �� �������� ������� � �������� �������
		*(sig_real_0+i+mas_size/2) = *(sig_real_temp+i); // ������ ��������� � �������� ������� �� �������� � ������

		*(sig_real_temp+i+1) = *(sig_real_0+i+1);
		*(sig_real_0+i+1) = *(sig_real_0+i_2-1);
		*(sig_real_0+i+1+mas_size/2) = *(sig_real_temp+i+1);

		*(sig_real_temp+i+2) = *(sig_real_0+i+2);
		*(sig_real_0+i+2) = *(sig_real_0+i_2-2);
		*(sig_real_0+i+2+mas_size/2) = *(sig_real_temp+i+2);

		*(sig_real_temp+i+3) = *(sig_real_0+i+3);
		*(sig_real_0+i+3) = *(sig_real_0+i_2-3);
		*(sig_real_0+i+3+mas_size/2) = *(sig_real_temp+i+3);

		*(sig_real_temp+i+4) = *(sig_real_0+i+4);
		*(sig_real_0+i+4) = *(sig_real_0+i_2-4);
		*(sig_real_0+i+4+mas_size/2) = *(sig_real_temp+i+4);

		*(sig_real_temp+i+5) = *(sig_real_0+i+5);
		*(sig_real_0+i+5) = *(sig_real_0+i_2-5);
		*(sig_real_0+i+5+mas_size/2) = *(sig_real_temp+i+5);

		*(sig_real_temp+i+6) = *(sig_real_0+i+6);
		*(sig_real_0+i+6) = *(sig_real_0+i_2-6);
		*(sig_real_0+i+6+mas_size/2) = *(sig_real_temp+i+6);

		*(sig_real_temp+i+7) = *(sig_real_0+i+7);
		*(sig_real_0+i+7) = *(sig_real_0+i_2-7);
		*(sig_real_0+i+7+mas_size/2) = *(sig_real_temp+i+7);


		//***********IMAG***********
		*(sig_imag_temp+i) = *(sig_imag_0+i); // ����������� ��������� � ������ �� ��������� ������
		*(sig_imag_0+i) = *(sig_imag_0+i_2); // ������ ��������� � ������ �� �������� ������� � �������� �������
		*(sig_imag_0+i+mas_size/2) = *(sig_imag_temp+i); // ������ ��������� � �������� ������� �� �������� � ������

		*(sig_imag_temp+i+1) = *(sig_imag_0+i+1);
		*(sig_imag_0+i+1) = *(sig_imag_0+i_2-1);
		*(sig_imag_0+i+1+mas_size/2) = *(sig_imag_temp+i+1);

		*(sig_imag_temp+i+2) = *(sig_imag_0+i+2);
		*(sig_imag_0+i+2) = *(sig_imag_0+i_2-2);
		*(sig_imag_0+i+2+mas_size/2) = *(sig_imag_temp+i+2);

		*(sig_imag_temp+i+3) = *(sig_imag_0+i+3);
		*(sig_imag_0+i+3) = *(sig_imag_0+i_2-3);
		*(sig_imag_0+i+3+mas_size/2) = *(sig_imag_temp+i+3);

		*(sig_imag_temp+i+4) = *(sig_imag_0+i+4);
		*(sig_imag_0+i+4) = *(sig_imag_0+i_2-4);
		*(sig_imag_0+i+4+mas_size/2) = *(sig_imag_temp+i+4);

		*(sig_imag_temp+i+5) = *(sig_imag_0+i+5);
		*(sig_imag_0+i+5) = *(sig_imag_0+i_2-5);
		*(sig_imag_0+i+5+mas_size/2) = *(sig_imag_temp+i+5);

		*(sig_imag_temp+i+6) = *(sig_imag_0+i+6);
		*(sig_imag_0+i+6) = *(sig_imag_0+i_2-6);
		*(sig_imag_0+i+6+mas_size/2) = *(sig_imag_temp+i+6);

		*(sig_imag_temp+i+7) = *(sig_imag_0+i+7);
		*(sig_imag_0+i+7) = *(sig_imag_0+i_2-7);
		*(sig_imag_0+i+7+mas_size/2) = *(sig_imag_temp+i+7);

		i_2 -= 8;
	}

	delete []sig_real_temp;
	delete []sig_imag_temp;

	return true;
}


/* ��������������� ������� �������� ������� ������� ������ � ����� ��� ����������� ��������� ��������� ��������������� ����� */
/*
������� ���������:
	*in_mas   - ��������� �� ������� ���������� ������ ������, ��� float;
	rx_num    - ���������� ���������, ��� int; 
	ps_num    - ���������� ��������� � �����, ��� int; 
	smp_num   - ���������� �������� ���������� ������ � ��������, ��� int; 
	//����������� ���� ��������� ������ ��������� ������� ������//
	**out_mas - ��������� ��������� �� �������� ��������� ������, ��� float;
	len_b     - ���������� �������� ��� � �������� �������� �������, ��� int;
	len_a     - ���������� ��������� �� ������� �������, ��� int;

*/
void buffer_3d(float* in_mas, float** out_mas, int len_a, int len_b, int rx_num, int ps_num, int smp_num){
	for (register int i = 0; i<rx_num; i++){
		for (register int j = 0; j<ps_num; j++){
			for (register int k = 0; k<smp_num; k++){
				*( *(out_mas + i) + j*smp_num + k) = *(in_mas + len_b*len_a*i + len_b*j + k);
			}
		}
	}
}

bool save_npy(float *sig_real_0, float *sig_imag_0, int n,string path)
{
	ofstream myReadFile;
	myReadFile.open(path);
	if (myReadFile.is_open())
	{
		for (int i = 0; i < n; i++)
		{
			myReadFile<<" ";
			myReadFile<<scientific<<setprecision(4)<<sig_real_0[i];
			myReadFile<<" ";
			myReadFile<<scientific<<setprecision(4)<<sig_imag_0[i];
			myReadFile<<" ";
			
			myReadFile<<"\n";
		}

		myReadFile.close();
		return true;
	}
	else
	{
		return false;
	}
	

}

bool save_npy_2d(float **sig_real_0, float **sig_imag_0, int n, int m, string path)
{
	ofstream myReadFile;
	myReadFile.open(path);
	if (myReadFile.is_open())
	{
		for (int register j = 0; j < m; j++)
		{
			for (int register i = 0; i < n; i++)
			{
				myReadFile<<" ";
				myReadFile<<scientific<<setprecision(18)<<*( *(sig_real_0 + j) + i);
				myReadFile<<" ";
				myReadFile<<scientific<<setprecision(18)<<*( *(sig_imag_0 + j) + i);
				myReadFile<<" ";
			
			}

			myReadFile<<"\n";
		}

		myReadFile.close();
		return true;
	}
	else
	{
		return false;
	}
	

}


