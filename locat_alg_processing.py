# -*- coding: utf-8 -*-
"""
Created on Tue May 29 17:38:54 2018

@author: belyaev
"""

import numpy as np

from scipy import constants

import cv2 as cv
import matplotlib.pyplot as plt

################################

#T_sw = 3.5e-5
T_adc = 25.6e-6
T_puls = 30e-6
f_d = 10e6
f_c = 77e9

sol = 3e8
sof = 1e13

################################
##Задание начальных параметров##
################################
#Xt = (np.load('IF_signal_model.npy'))
sampls = int(T_adc*f_d)
pulses = 256
H = pulses

num = 200

dD = sol/(4*sof*T_adc)
Dmax = (f_d*sol * T_adc)/(2*sof*T_adc)
d_axe = np.arange(0,Dmax,dD)

Vmax = sol/(2*T_puls*f_c)
dV = sol/(2*f_c*T_puls*H)

df = 1/37
Pi = np.pi

freqq0 = np.arange(-0.87,0.87,1*df/2)
teth_axe0 = np.zeros(len(freqq0),float)
teth_axe0[:] = np.arcsin( (freqq0[:]))*180/Pi

resiv = 128

plt.close("all")
        
xt = np.loadtxt('D:\\work\\locat\\FMCW-radar\\FMCW_radar\\model.txt').view(complex)
xt = np.reshape(xt, (8, pulses, sampls)) 

Xt0 = np.fft.fftn(xt,(resiv, pulses, 512))

print(Xt0.shape)
            
for i in range(resiv):
    for j in range(sampls):
        Xt0[i,:,j] = np.fft.fftshift(Xt0[i,:,j])
        
for i in range(pulses):
    for j in range(sampls):
        Xt0[:,i,j] = np.fft.fftshift(Xt0[:,i,j])
        
Pi2 = 9.869604401089358

koef = 16*dD*dD*Pi2
Xt0 = abs(Xt0)

cmap = plt.get_cmap('jet')
plt.figure(0)
X, Y = np.meshgrid(d_axe, teth_axe0)
plt.pcolormesh(X*np.sin(np.radians(Y)), X*np.cos(np.radians(Y)), Xt0[:,89,:], cmap = cmap)
plt.title('2D FFT ESTIMATE Rx-Ry')
plt.xlabel('range Y')
plt.ylabel('range X')
plt.ylim([0,150])