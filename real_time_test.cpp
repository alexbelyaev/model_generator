#include "real_time_test.h"

#include <iostream>
#include <ctime>
#include <fstream>
#include <cstdio>
#include <vector>

using namespace std;

int main(int) {

	snr_params snr;
	Signal_params signal;
  
	string il;


	// ==========================================================================================
	// �������� ���������� ������� �� ����� 
	string signal_file = "resources/model_params.cfg";
	int signal_model_status = params_reader(signal, signal_file);
	if (signal_model_status)
		return 1;
	else
		signal._init = 1;


	// ������������� ��������� ���������� ��������� � ����������� �������
	init_Signal_params(signal);
	int rx_num  = signal._rx_num;
	int ps_num  = signal._ps_num;
	int smp_num = signal._smp_num;

	float *inp = new  float [rx_num*ps_num*smp_num];
	float *qdr = new  float [rx_num*ps_num*smp_num];

	float *SNR_inp = new  float;
	float *SNR_qdr = new  float;

  // ������ ���������� �� ������� �� �����
	//int i = 12;

	for (int i = 41; i < 45; i++){
		string pass_input = "C:\\Users\\belyaev.INTEGRANT\\source\\repos\\model_generator\\model_generator\\targets\\";
		string number_input = to_string(i);
		string name_input = "_targets.txt";
		string targets_file = pass_input + number_input + name_input;
		cout<<"test_in";
		cout<<'\n';
		vector <vector <double>> targets(5, vector<double>());
		int targets_model_status = targets_reader(targets, targets_file);
		if (targets_model_status)
			return 1;
		int targets_num = targets[0].size();

		// ==========================================================================================
		// ���� ���������� ������ ������������� ������� 
		// ( ������ ���� �������� ��� ���� ��������� � ���������)


		get_signal_n_model(inp, qdr, snr, signal, targets, targets_num);

		string pass_output = "C:\\Users\\belyaev.INTEGRANT\\source\\repos\\model_generator\\model_generator\\targets_out\\";
		string number_output = to_string(i);
		string name_output = "_model.txt";

		string ouput_model_signal = pass_output + number_output + name_output;

		cout<<"\n\n";
		cout<<"RMS noise inphase    = "<<snr.RMS_n_real<<" V \n";
		cout<<"RMS noise quadrature = "<<snr.RMS_n_imag<<" V \n";
		cout<<"RMS signal inphase   = "<<snr.RMS_s_real<<" V \n";
		cout<<"RMS signal quadrature = "<<snr.RMS_s_imag<<" V \n";
		cout<<"SNR_inp = "<< snr.SNR_inp<<" dB \n";
		cout<<"SNR_qdr = "<< snr.SNR_qdr<<" dB \n";
		save_npy(inp,qdr,smp_num*ps_num*rx_num, ouput_model_signal);


		cout<<"test_out";
		cout<<'\n'<<number_input;
		cout<<'\n'<<'\n';
	}

	cin.get();
	return 0;
}